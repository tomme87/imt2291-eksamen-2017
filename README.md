# My project's README

Studentnummer: 223686

Øverst i de fleste filer ligger oppgaveteksten, det er mest for min egen del, så du behøver ikke lese det.

**Oppgave 12 D:** for å få overført sessjonscookie'en riktig mellom klient og server i dette tifellet må man sørge for at server sender riktig headere for 
CORS (Cross-origin resource sharing) som forteller nettleseren hvilke domener den har lov til å nå (Access-Control-Allow-Origin).