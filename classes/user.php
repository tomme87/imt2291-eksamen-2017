<?php
/**
 * Enkel brukerklasse for å holde orden på bruker.
 *
 * Flere ting her er hentet fra imt2291.
 *
 * @author imt2291+tom
 */
class User {
  var $id = -1;
  var $username = null;
  var $db;
  var $unknownUser = null;
  
  /**
   * Constructor
   * Fyller inn data om bruker er logget inn.
   *
   * @param PDO $db database objekt.
   */
  function User(PDO $db) {
    $this->db = $db; // Database object.
    if (isset($_SESSION['uid'])) { // A user is logged in
      $sql = "SELECT id, uname FROM user WHERE id=?";
      $sth = $this->db->prepare($sql);
      $sth->execute (array ($_SESSION['uid'])); // Find user information from the database
      if ($row = $sth->fetch()) {         // User found
        $this->id = $row['id'];      // Store user id in object
        $this->username = $row['uname'];
      } else {                  // No such user
        unset ($_SESSION['uid']);       // Remove user id from session
      }
    }
  }
  
  /**
   * This method returns true if a user is logged in, false if no 
   * user is logged in.
   * 
   * @return boolean value of true if a user is logged in, false if no user is logged in.
   */
  function isLoggedIn() {
    return ($this->id > -1); // return true if userid > -1
  }
  
  /**
   * Funksjon som logger inn brukeren
   * Data blir hentet fra _POST
   *
   * @return boolean om bruker greide å logge inn.
   */
  function login() {
    // Hent brukerinfo fra database.
    $sql = "SELECT id, uname, pwd FROM user WHERE uname = ?";
    $sth = $this->db->prepare($sql);
    $sth->execute(array($_POST['username']));
    
    if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // Brukeren finnes
      if (password_verify ($_POST['password'], $row['pwd'])) { // Passord stemmer
        $_SESSION['uid'] = $row['id']; // Setter sesjonsvariabel med ID-en til brukeren
        $this->username = $row['uname'];
        $this->id = $row['id'];
      } else {
        $this->unknownUser = true;
        return false; // Passord stemmer ikke
      }
    } else {
      $this->unknownUser = true;
      return false; // Ukjent bruker.
    }
    return true; // Login OK.
  }
  
  /**
   * Logger ut bruker. og resetter variabler
   */
  function logout() {
    unset ($_SESSION['uid']); // Fjern sesjonsvariabel.
    // resett variablene.
    $this->id = -1;
    $this->username = null;
  }
  
  /**
   * Registrer ny bruker.
   */ 
  function register() {
    $sql = 'INSERT INTO user (uname, pwd) VALUES (?, ?)';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($_POST['username'], password_hash($_POST['password'], PASSWORD_DEFAULT)));
    if ($sth->rowCount()==0) {	// If unable to create user
      $this->unknownUser = true;
      return false; // Ukjent bruker.
    }
    
    $this->id = $this->db->lastInsertId();
    $this->username = $_POST['username'];
    $_SESSION['uid'] = $this->id;
    return true;
  }
  
  /**
   * Resizer avatar til maks 150x150 og lagrer til database.
   * Source: http://php.net/manual/en/function.imagecopyresized.php
   */
  function saveAvatar($file) {
    list($orig_width, $orig_height) = getimagesize($file);
    
    $width = $orig_width;
    $height = $orig_height;
    
    if ($height > 150) {
      $width = (150 / $height) * $width;
      $height = 150;
    }
    
    if ($width > 150) {
      $height = (150 / $width) * $height;
      $width = 150;
    }
    
    $image_p = imagecreatetruecolor($width, $height);
    $image = imagecreatefromjpeg($file);
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);
    
    // Lagre som jpeg til variabel. Source: https://stackoverflow.com/questions/11446008/php-how-to-create-a-string-of-image-binary-without-saving-it-to-a-file
    ob_start(); //Stdout --> buffer
    imagejpeg($image_p);
    $imgString = ob_get_contents(); //store stdout in $imgString
    ob_end_clean(); //clear buffer
    imagedestroy($image_p); //destroy img
    
    $sql = 'UPDATE user SET avatar = ? WHERE id = ?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($imgString, $this->id));
    if ($sth->rowCount()==0) { // greide ikke lagre.
      return false;
    } 
    
    return true;
  }
}