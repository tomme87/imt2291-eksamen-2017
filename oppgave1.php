<?php
/*
Lag et PHP skript (oppgave1.php) som oppretter en database med navn "IMT2291Eksamenstudentnr" (altså ditt studentnummer til slutt i navnet til databasen), 
dette er den databasen du vil bruke gjennom hele oppgavesettet. I denne tabellen skal skriptet videre opprette en tabell "user" med kolonnene :
  id: bigint, primary key, auto increment, not null
  uname: varchar 32, unique, not null
  pwd: varchar 255, not null
  avatar: blob
Legg til en bruker med brukernavn "test" og passord "test". Passordet skal hashes med php funksjonen password_hash.
*/

$debug = false;
$dbname = 'IMT2291Eksamen223686';
$dbuser = 'root';
$dbpass = '';
$dbhost = '127.0.0.1';

$user = 'test';
$password = 'test';

try {	// Prøv å koble til database
	$db = new PDO('mysql:host='.$dbhost.';',$dbuser,$dbpass);
} catch (PDOException $e) {	// Ved feil
  if(!(isset($install) && $install)) {
    if (isset($debug) && $debug)		// Hvis degugging
      die('Unable to connect to database : '.$e->getMessage());
    else 					// hvis ikke
      die('Unable to connect to database, please try again later.');
  }
}

// Lag database
$db->exec("CREATE DATABASE IF NOT EXISTS `$dbname`");

// Bruk denne databasen.
$db->exec("use $dbname");

// Lag user tabell
$sql = "CREATE TABLE IF NOT EXISTS `user` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `uname` varchar(32) COLLATE utf8_bin NOT NULL,
 `pwd` varchar(255) COLLATE utf8_bin NOT NULL,
 `avatar` blob,
 PRIMARY KEY (`id`),
 UNIQUE KEY `uname` (`uname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin";
$db->exec($sql);

// Legg til en bruker i databasen med hashet passord.
$sql = "INSERT INTO user (uname, pwd) VALUES (?, ?)";
$sth = $db->prepare($sql);
$sth->execute(array($user, password_hash($password, PASSWORD_DEFAULT)));
if ($sth->rowCount()==0) { // Bruker finnes.
  die("Brukeren er allerede lagt inn.");
}

echo "Done!";
