<?php
/*
Fra http://api.geonames.org/postalCodeSearchJSON?placename=gj%C3%B8vik&country=NO&maxRows=30&username=demo vil du få en oversikt over alle postnummer i Gjøvik. 
Les disse dataene og skriv ut en tabell med postnummer og stedsnavn (placename). Ta kun med steder i Oppland. Bruk en tabell for å vise postnummer/stedsnavn.

Det er en begrensning på 2000 forespørsler pr. time pr. brukernavn. I tillegg til den globale brukeren demo så har jeg også opprettet en bruker med brukernavn "okolloen" 
men også der kan antall forespørsler pr. time gå over 2000. Gå til http://www.geonames.org/login for å opprette et eget brukernavn til bruk istedenfor "demo"/"okolloen".
*/

require_once 'include/header.php'; // User-klasse blir inkludert her samt $user objektet.

$place = 'gjøvik'; // Hvor skal vi søke
$username = 'okolloen'; // Brukernavn hos geonames

$content = file_get_contents('http://api.geonames.org/postalCodeSearchJSON?placename='.urlencode($place).'&country=NO&maxRows=30&username='.urlencode($username));
$postArray = json_decode($content);

?>
<div class="table-responsive"> <!-- Slik at den kan vises på alle skjermer. -->
  <table class="table table-striped">
    <thead>
      <th>postnummer</th><th>stedsnavn</th>
    </thead>
    <tbody>
      <?php
      foreach($postArray->postalCodes as $res) {
        if ($res->adminName1 == 'Oppland') { // Vil kun ha steder i oppland.
          echo '<tr><td>'.$res->postalCode.'</td>';
          echo '<td>'.htmlspecialchars($res->placeName).'</td>';
        }
      }
      ?>
    </tbody>
  </table>
</div>
<?php

require_once 'include/footer.php';