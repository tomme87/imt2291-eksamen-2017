<?php
/*
Lag et PHP skript (oppgave2.php) som første gang viser en form hvor brukeren skal skrive inn brukernavn og passord (bruk <label> for å gi ledetekst til feltene). 
Bruk CSS for å gi fornuftig formatering til formen. Når brukeren trykker på knappen for å logge på skal brukernavn og passord sendes til samme skriptet. 
Sjekk om brukeren finnes i databasen. Dersom brukeren ikke finnes i databasen så vis formen på nytt med brukernavnet fylt ut men uten passordet fylt ut. 
Gi brukeren tilbakemelding om at bruker med oppgitt brukernavn/passord kombinasjon ikke finnes i databasen.

Dersom brukeren finnes skal det settes en sessjonsvariabel med id'en til brukeren. 
Videre skal det vises en velkomstmelding med brukernavnet til brukeren samt en knapp for å kunne logge ut.

Dersom denne siden (oppgave2.php) vises mens en bruker er logget på skal også denne meldingen og knappen vises. 
Du må altså sjekke sessjonsvariabel for å finne ut om formen eller denne meldingen skal vises.

Når brukeren trykker på knappen for å logge ut skal igjen samme skriptet kalles og sessjonsvariabelen slettes. 
Brukeren skal da få frem formen for å fylle inn brukernavn og passord.
*/
require_once 'include/header.php'; // User-klasse blir inkludert her samt $user objektet.

if(isset($_GET['logout'])) { // Brukeren logger ut.
  $user->logout();
  header("Location: oppgave2.php"); // Send bruker tilbake uten logout i url.
  die;
}

if(isset($_POST['username'])) { // Bruker forsøker å logge inn.
  $user->login(); // Logger inn.
}


if(!$user->isLoggedIn()) { // Bruker er ikke logget inn.
?>
<div class="row">
  <div class="col-md-6 col-md-offset-3"> <!-- For å få det litt penere (ikke så bredt) -->
    <?php
    if($user->unknownUser) { // Bruker finnes ikke.
      echo '<div class="alert alert-danger" role="alert">bruker med oppgitt brukernavn/passord kombinasjon finnes ikke i databasen</div>';
    }
    ?>
    <form action="oppgave2.php" method="POST">
      <div class="form-group">
        <label for="inpurUsername">Brukernavn</label>
        <input name="username" type="text" class="form-control" id="inputUsername" placeholder="Brukernavn" 
          value="<?php echo isset($_POST['username']) ? htmlspecialchars($_POST['username']) : '' ?>">
      </div>
      <div class="form-group">
        <label for="inputPassword">Passord</label>
        <input name="password" type="password" class="form-control" id="inputPassword" placeholder="Passord">
      </div>
      <button type="submit" class="btn btn-default">Logg inn</button>
    </form>
  </div>
</div>
<?php
} else {
  ?>
<div class="jumbotron">
  <h1>Velkommen!</h1>
  <p>hei <strong><?php echo htmlspecialchars($user->username); ?>!</strong></p>
  <p><a class="btn btn-primary btn-lg" href="?logout" role="button">Logg ut</a></p>
</div>
<?php
}

require_once 'include/footer.php';