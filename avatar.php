<?php
require_once 'include/db.php'; // koble til database.

if(isset($_GET['id']) && $_GET['id']) { // Hvis vi har f�tt ID
  $id = $_GET['id']; // Bruker ID
  
  // Hent bilde
  $sql = "SELECT avatar FROM user WHERE id = ?";
  $sth = $db->prepare($sql);
  $sth->execute(array($id));
  if ($row = $sth->fetch(PDO::FETCH_ASSOC)) { // Brukeren finnes
    if($row['avatar']) { // Bruker har avatar
      header('Content-Type: image/jpeg');
      echo $row['avatar'];
    } else {
      die("no avatar");
    }
  } else {
    die("no such user");
  }
} else {
  die("missing ID");
}
