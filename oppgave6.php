<?php
/*
oppgave6.php skal returnere en liste over alle brukere på JSON format. Det som skal returneres for hver bruker er brukerid, 
brukernavn og "y"/"n" for hvorvidt det finnes et avatarbilde eller ikke. JSON dataene skal returneres med Content-type = application/json.
*/
require_once 'include/db.php'; // koble til database.

// Hent alle brukere.
$sql = 'SELECT id, uname, avatar FROM user';
$sth = $db->prepare($sql);
$sth->execute();

header('Content-Type: application/json'); // Returnerer JSON

$users = array();
while($res = $sth->fetch(PDO::FETCH_ASSOC)) { // Loop gjennom brukerne å lag nytt array
  $users[] = array(
    'id' => $res['id'],
    'uname' => $res['uname'],
    'avatar' => $res['avatar'] ? 'y' : 'n'
  );
}
echo json_encode($users); // Konverter array til json