<?php
/*
I katalogen sql (i dette repositoriet) finnes filen studyPrograms.sql, importer denne inn i databasen du laget i oppgave 1. Du har nå fått tre nye tabeller :
studyprogram :        Inneholder navn på studieprogrammer.
studyprogramContent : Inneholder emner som inngår i et studieprogram. startYear er hvilket år studentene startet, 
                      dette da innholdet i et studieprogram vil variere avhengig av hvilket år en starter (det er ulike emner som inngår, 
                      emnene kan være plassert i ulike semestre.) Det er også merket om et emne er obligatorisk eller et valgemne.
subject :             Inneholder mer informasjon om de ulike emnene. 
                      For å koble et emne til et studieprogram må en altså finne emne med riktig emnekode og en må ta høyde for når emnet undervises. 
                      Hvert emne finnes altså på nytt for hvert år det er brukt i et studieprogram. URL'en kan/vil være ulik for samme emne undervist i ulike år, 
                      dette da innholdet i emnet kan endres uten at emnekoden endres.

Ut i fra den informasjonen som ligger i disse tabellene skal du lage oppgave8.php som genererer en oversikt over alle studieprogram med oppstart i 2016 
(alle=1, da det kun er registrert et studieprogram med oppstart i 2016.)
*/
require_once 'include/header.php'; // User-klasse blir inkludert her samt $user objektet.

$sql = 'SELECT id, name FROM studyprogram WHERE id IN(SELECT studyprogram FROM studyprogramContent WHERE startYear = 2016)';
$sth = $db->prepare($sql);
$sth->execute();
while($res = $sth->fetch(PDO::FETCH_ASSOC)) { // Loop gjennom alle studieprogram
  echo '<h1>'.htmlspecialchars($res['name']).'</h1>';
  ?>
  <div class="table-responsive"> <!-- Slik at den kan vises på alle skjermer. -->
    <table class="table table-striped">
      <thead>
        <tr><th>Emnekode</th><th>Emnenavn</th><th>O/V</th><th colspan="6">Studiepoeng per semester</th></tr>
        <tr><th colspan="3"></th><th>S1(h)</th><th>S1(h)</th><th>S1(h)</th><th>S1(h)</th><th>S1(h)</th><th>S1(h)</th></tr>
      </thead>
      <tbody>
        <?php
        // Finn studieprogram-content
        $sql_sc = 'SELECT subject, semester, type, startYear FROM studyprogramContent WHERE studyprogram = ? AND startYear = 2016 ORDER BY semester, subject';
        $sth_sc = $db->prepare($sql_sc);
        $sth_sc->execute(array($res['id']));
        
        while($res_sc = $sth_sc->fetch(PDO::FETCH_ASSOC)) { // Loop gjennom
          // finn tilhørende subject
          $sql_s = 'SELECT name, credits, semester FROM subject WHERE year = ? AND code = ?';
          $sth_s = $db->prepare($sql_s);
          $sth_s->execute(array($res_sc['startYear'] + floor($res_sc['semester']/2), $res_sc['subject'])); // Year = startÅr + semester/2(rundet ned)
          $res_s = $sth_s->fetch(PDO::FETCH_ASSOC);
          
          echo '<tr><td>'.$res_sc['subject'].'</td>'.'<td>'.$res_s['name'].'</td>'.'<td>'.$res_sc['type'].'</td>';
          for ($i = 1; $i <= 6; $i++) { // skriv poeng til riktig semester
            echo '<td>'.($res_sc['semester'] == $i ? $res_s['credits'] : '').'</td>';
          }
          echo '</tr>';
        }
        ?>
      </tbody>
    </table>
  </div>
  <?php
}

require_once 'include/footer.php';