<?php
/*
I oppgave5.php skal det generes en liste over alle registrerte brukere. Dersom brukeren har lastet opp et avatarbilde skal dette ogs� vises. 
Bruk Bootstrap for � formatere dataene slik at listen kan vises p� alt fra store skjermer til mobil. 
Lag skriptet avatar.php for � hente bilder fra databasen slik at disse kan vises.
*/
require_once 'include/header.php'; // User-klasse blir inkludert her samt $user objektet.

// Hent alle brukere.
$sql = 'SELECT id, uname, avatar FROM user';
$sth = $db->prepare($sql);
$sth->execute();
$results = $sth->fetchAll(PDO::FETCH_ASSOC);

?>
<div class="table-responsive"> <!-- Slik at den kan vises p� alle skjermer. -->
  <table class="table table-striped">
    <thead>
      <th>ID</th><th>Brukernavn</th><th>Avatar</th>
    </thead>
    <tbody>
      <?php
      foreach($results as $res) {
        echo '<tr><td>'.$res['id'].'</td>';
        echo '<td>'.htmlspecialchars($res['uname']).'</td>';
        if($res['avatar'])
          echo '<td><img src="avatar.php?id='.$res['id'].'" alt="avatar" /></td></tr>';
        else
          echo '<td>N/A</td></tr>';
      }
      ?>
    </tbody>
  </table>
</div>
<?php

require_once 'include/footer.php';