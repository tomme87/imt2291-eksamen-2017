<?php

?>
    </div> <!-- /.container -->
    <!-- Latest minified jQuery -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
      integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      
<?php
// Bruker samme m�te som jeg gjorde i prosjekt1 for � f� scriptet inkludert etter jQuery og bootstrap js.
if(isset($jsInc)) { // Legg til .js
  foreach($jsInc as $j) {
    echo "<script src=\"js/$j\"></script>\n";
  }
}
?>
      
  </body>
</html>