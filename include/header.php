<?php
/**
 * Header fil som bruker gjennom hele eksamen for � navigere mellom oppgaver.
 * Bruker bootstrap for at det skal se litt bedre ut. http://getbootstrap.com/
 * Source: http://getbootstrap.com/components/#navbar
 */
session_start(); // For � kunne bruke sessions.
require_once 'db.php'; // koble til database.
require_once 'classes/user.php'; // user klassen.

$user = new User($db); // Se classes/user.php
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IMT2291 - Eksamen 2017</title>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
      
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
      integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />
      
  </head>
  <body>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">IMT2291 Eksamen 2017</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Oppgaver <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="oppgave1.php">Oppgave 1</a></li>
                <li><a href="oppgave2.php">Oppgave 2</a></li>
                <li><a href="oppgave3.php">Oppgave 3</a></li>
                <li><a href="oppgave4.php">Oppgave 4</a></li>
                <li><a href="oppgave5.php">Oppgave 5</a></li>
                <li><a href="oppgave6.php">Oppgave 6</a></li>
                <li><a href="oppgave7.php">Oppgave 7</a></li>
                <li><a href="oppgave8.php">Oppgave 8</a></li>
                <li><a href="oppgave9.php">Oppgave 9</a></li>
                <li><a href="oppgave10.html">Oppgave 10</a></li>
                <li><a href="oppgave11.html">Oppgave 11</a></li>
                <li><a href="oppgave12.html">Oppgave 12</a></li>
                <li><a href="http://localhost:8000/components/oppgave13/">Oppgave 13</a></li>
              </ul>
            </li>
          </ul>
        </div> <!-- /.navbar-collapse -->
      </div> <!-- /.container-fluid -->
      
    </nav>
    
    <!-- Start hoved-container -->
    <div class="container">
    