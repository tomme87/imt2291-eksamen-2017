<?php
/*
Lag et PHP skript (oppgave3.php) som kan brukes for å opprette en ny bruker i tabellen vi har brukt så langt. 
Brukeren skal oppgi brukernavn og passord samt bekrefte passordet. Brukernavn må være minst seks tegn og passordet må være minst åtte tegn. 
Begge passord må selvfølgelig være like. Ikke la brukeren sende formen før disse tre betingelsene er oppfylt. 
Når brukeren har skrevet korrekt brukernavn og passord skal dataene sendes til samme skriptet og registreres i databasen. 
Dersom brukeren kan opprettes (det vil si at det ikke er duplisering av brukernavn) 
så setter du sessjonsvariabelen som i oppgave 2 og sender brukeren videre til oppgave2.php. 
Dersom brukeren ikke kan opprettes så viser du formen på nytt med brukernavnet fylt ut og en melding til brukeren om at dette brukernavnet er opptatt 
og at nytt brukernavn må velges.
*/
require_once 'include/header.php'; // User-klasse blir inkludert her samt $user objektet.

$alert = false;
if(isset($_POST['username'])) { // Bruker forsøker å registrere seg.
  $reg = $user->register();
  if(!$reg) {
    $alert = true;
  } else { // Registrering fullført
    header("Location: oppgave2.php"); // Send bruker oppgave2.
  }
}

?>

<div class="row">
  <div class="col-md-6 col-md-offset-3"> <!-- For å få det litt penere (ikke så bredt) -->
    <?php
    if($alert) { // Vis alert ved feil
      echo '<div class="alert alert-danger" role="alert">Brukeren kunne ikke opprettes. Brukernavn finnes fra før.</div>';
    }
    ?>
    <div style="display: none;" class="alert alert-danger" role="alert" id="formError"></div>
    <form id="registerForm" action="oppgave3.php" method="POST">
      <div class="form-group">
        <label for="inpurUsername">Brukernavn</label>
        <input name="username" type="text" class="form-control" id="inputUsername" placeholder="Brukernavn" 
          value="<?php echo isset($_POST['username']) ? htmlspecialchars($_POST['username']) : '' ?>">
      </div>
      <div class="form-group">
        <label for="inputPassword1">Passord</label>
        <input name="password" type="password" class="form-control" id="inputPassword1" placeholder="Passord">
      </div>
      <div class="form-group">
        <label for="inputPassword2">Bekreft passord</label>
        <input name="password_confirm" type="password" class="form-control" id="inputPassword2" placeholder="Bekreft passord">
      </div>
      <button type="submit" class="btn btn-default">Registrer deg</button>
    </form>
  </div>
</div>


<?php
$jsInc = array('oppgave3.js'); // Se js/oppgave3.js for håndtering av submit
require_once 'include/footer.php';