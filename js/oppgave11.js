/*
I repositoriet finner du oppgave11.html, fyll inn den javascript koden som skal til for � foreta et AJAX kall til http://imt2291.hig.no/eksamen2017/jsonData.php 
og bruke dataene du f�r tilbake derfra for � f� utseende fra skjermdumpen under. Fyll inn <h1> taggen og tittelen p� siden med navnet p� studiet, 
og alle emnene med studiepoeng i riktig studie�r og semester.
*/
// Source: http://api.jquery.com/jquery.getjson/ - http://api.jquery.com/append/

$(function() { // On ready.
  $.getJSON('http://imt2291.hig.no/eksamen2017/jsonData.php', function(data) {
    console.log(data);
    $('h1').text(data[0].studyprogram); // Legg inn overskrift.
    data.forEach(function (subject) { // For hvert element..
      var table = $('table').eq(Math.ceil(subject.semester/2)-1).children('tbody'); // Finn tabell (tbody) for riktig studie�r.
      
      // Lag rad.
      var tr = '<tr><td class="code">' + subject.subject + '</td>'
        + '<td class="name">' + subject.name + '</td>'
        + '<td class="type">' + (subject.type == 'obligatory' ? 'O' : 'V') + '</td>';
      
      for (i = 1; i <= 6; i++) { // Sjekk hvilket semester den skal skrives til.
        tr += '<td>' + (parseInt(subject.semester) == i ? subject.credits : '') + '</td>';
      }
      tr += '</tr>';
      
      $(table).append(tr); // Legg til rad.
    });
  });
});