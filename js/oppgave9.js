/**
 * Creates a new object with firstname, lastname and birthdate.
 * @param fname the given name of the person to create
 * @param lname the last name (family name) of the person to create
 * @param bdate the birthdate of the person to create, used by the sayHello function to say happy birthday on the right day of the year
 */
function Person (fname, lname, bdate) {
  // lagre til objekt
  this.firstName = fname;
  this.lastName = lname;
  this.birthDay = bdate;
  
  this.sayHello = function() {
    // Sjekk om bursdag er i dag. Source: https://stackoverflow.com/questions/8215556/how-to-check-if-input-date-is-equal-to-todays-date
    var today = new Date();
    if(today.toDateString() == this.birthDay.toDateString()) {
      alert("Gratulerer med dagen " + this.firstName + ' ' + this.lastName);
    } else {
      alert("Hei på deg " + this.firstName + ' ' + this.lastName);
    }
  };
}

// Kode fra oppgaven
var p1 = new Person ("Hansen", "Hansen", new Date());
p1.sayHello();

var tomorrow = new Date(new Date().getTime()+1000*60*60*24);
var p2 = new Person ("Ole", "Olsen", tomorrow);
p2.sayHello();