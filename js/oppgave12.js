/*
Oppgave 12 A, B og C
*/

var URL = 'http://imt2291.hig.no/eksamen2017/login.php'; // Til login siden.

$(function() { // On ready. - Oppgave A.
  $.getJSON(URL + '?loginstatus=true', function(data) { // GET med loginstatus=true
    if(data.status == 'loggedIn') { // logget inn.
      $('div#loggedin').show().find('h1 span').text(data.name); // Vis velkomst og sett inn navn
    } else if (data.status == 'loggedOut') { // Ikke logget inne
      $('div#login').show(); // Vis login
    }
  });
  
  // Oppgave B.
  $('div#login input[type=button]').click(function() { // N�r vi klikker logg inn.
    // Hent bruker/pass fra input.
    var name = $('#username').val();
    var pwd = $('#password').val();
    
    $.post(URL, {uname: name, pwd: pwd}, function(data) { // POST med uname=brukernavn og pwd=passord.
      if(data.status == 'OK') { // Logget inn.
        $('div#login').hide(); // Skjul login.
        $('div#loggedin').show(); // Vis velkomst.
      } else if (data.status == 'FAIL') { // IKKE logget inn.
        $('#warning').text(data.message).show(); // Sett inn feilmelding og vis den.
      }
    }, 'json');
  });
  
  // Oppgave C.
  $('div#loggedin button').click(function() {
    $.getJSON(URL + '?logout=true', function(data) { // GET med logout=true
      if(data.status == 'OK') { // Logget ut.
        $('#warning').hide(); // Skjul evt gammel feilmelding.
        $('div#login').show(); // Vis login.
        $('div#loggedin').hide(); // Skjul velkomst.
      }
    });
  });
  
});