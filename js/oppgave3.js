/**
 * Bruker for å fylle følgende krav:
 *  - Brukernavn må være minst seks tegn og passordet må være minst åtte tegn.
 *  - Begge passord må selvfølgelig være like.
 */ 

$('#registerForm').submit(function(e) {
  
  var uname = $('#inputUsername').val(); // Brukernavn
  var pw = $('#inputPassword1').val(); // Passord 1
  var pw2 = $('#inputPassword2').val(); // Passord 2
  var errors = []; // Hold feilmeldinger.

  if(pw != pw2) { // Passord 1 er ikke likt passord 2.
    errors.push('Passordene er ikke like');
  }
  if (uname.length < 6) { // Brukernavn må være minst seks tegn.
    errors.push('Brukernavn må være minst 6 tegn');
  }
  if (pw.length < 8) { // Passord må være minst 8 tegn.
    errors.push('Passord må være minst 8 tegn');
  }
  
  if(errors.length) { // Vi har fått feil.
    $('#formError').text(errors.join(', ')).show(); // Vis feil i alert boksen.
    e.preventDefault(); // Ikke send form
  }
  
});