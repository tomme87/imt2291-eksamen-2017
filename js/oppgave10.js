/*
Plasseringen er høyrejustert, antall gull, sølv og bronsemedaljer er sentrert og totalt antall medaljer er høyrejustert. 
Kolonne 1 har fått høyrepadding på 5 punkter, kolonne 2 har bredde 100 punkter mens kolonne 3-5 har bredde 30 punkter. 
Den første raden har bakgrunnsfarge #DDD.
*/
// Source: https://api.jquery.com/nth-child-selector/ - http://api.jquery.com/css/

var rows = $('table tbody tr'); // Alle radene.

// Kolonne 1 - Plassering
$(rows).find('td:nth-child(1)').css({
  'text-align': 'right',
  'padding-right': '5px'
});

// Kolonne 2 - Land
$(rows).find('td:nth-child(2)').css({
  'width': '100px'
});

// Kolonne 3-5 - Gull/Sølv/Bronse
$(rows).find('td:nth-child(3), td:nth-child(4), td:nth-child(5)').css({
  'text-align': 'center',
  'width': '30px'
});

// Kolonne 6 - Total
$(rows).find('td:nth-child(6)').css({
  'text-align': 'right'
});

// Første rad.
$('table tbody tr:first').css('background-color', '#DDD');