<?php
/*
Når brukeren er logget på (enten ved å ha opprettet ny bruker eller logget på ved å kjøre oppgave2.php) 
så skal vedkommende ved å gå til oppgave4.php få mulighet til å laste opp avatar bilde (bilde for brukeren). 
Lag en form for å la brukeren laste opp bilde. Når brukeren sender bilde så skal dette sendes til samme skriptet. 
Skaler bildet til maksimalt 150x150 punkter og lagre det i tabellen i kolonnen avatar for riktig bruker.
*/

require_once 'include/header.php'; // User-klasse blir inkludert her samt $user objektet.

if ($user->isLoggedIn()) { // Må være logget inn for å laste opp.

if (isset($_FILES['inputFile']['name'])) {
  if ($user->saveAvatar($_FILES['inputFile']['tmp_name'])) { // Avatar saved
    echo '<div class="alert alert-success" role="alert">Avatar lagret!</div>';
  } else {
    echo '<div class="alert alert-danger" role="alert">Avatar ble ikke lagret! (Kan være fordi du hadde samme avatar fra før))</div>';
  }
}

?>

<div class="row">
  <div class="col-md-6 col-md-offset-3"> <!-- For å få det litt penere (ikke så bredt) -->
    <form id="uploadForm" action="oppgave4.php" enctype="multipart/form-data" method="POST" role="form">
      <div class="form-group">
        <label for="inputFile">Avatar</label>
        <input name="inputFile" type="file" id="inputFile">
        <p class="help-block">her kan du laste opp et avatar bilde (bilde for brukeren din).</p>
      </div>
      <button type="submit" class="btn btn-default">Last opp</button>
    </form>
  </div>
</div>

<?php
} else {
  echo '<p class="bg-danger">Ikke logget inn.</p>';
}
require_once 'include/footer.php';